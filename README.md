# Recruitment showcase backend #

This project has been created for recruitment purposes. It serves as a dummy backend for [RecruitmentShowcase](https://bitbucket.org/joke1410/recruitmentshowcase) project.

### Key notes

* The data is kept in memory so it's available until the app is running.
* The endpoints are available without any kind of authorization.
* The data is shared across all users.

