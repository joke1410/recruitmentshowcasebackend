//
//  File.swift
//  
//
//  Created by Piotr Bruź (PBRU) on 10/04/2022.
//

import Vapor

protocol Identifiable {
    var id: String { get }
}

struct Todo: Content, Identifiable {
    let id: String
    var content: String
    var moreDetails: String?
    let createdAt: Date
    var updatedAt: Date
    var isDone: Bool
}

struct TodoV2: Content, Identifiable {
    let id: String
    var content: String
    var moreDetails: String?
    let createdAt: Date
    var updatedAt: Date
    var isDone: Bool
    var priority: Int
}

struct GetTodosResponse<TodoModel: Content & Identifiable>: Content {
    let todos: [TodoModel]
    let deletedTodoIds: [String]
}
