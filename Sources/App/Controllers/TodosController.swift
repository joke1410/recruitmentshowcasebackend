//
//  TodosController.swift
//

import Vapor

final class TodosController<TodoModel: Content & Identifiable> {
    
    var inMemoryTodos: [TodoModel] = []
    var inMemoryDeletedTodoIds: Set<String> = []
    
    func get(req: Request) throws -> EventLoopFuture<GetTodosResponse<TodoModel>> {
        let response = GetTodosResponse(
            todos: inMemoryTodos,
            deletedTodoIds: Array(inMemoryDeletedTodoIds)
        )
        return req.eventLoop.makeSucceededFuture(response)
    }
    
    func create(req: Request) throws -> EventLoopFuture<TodoModel> {
        let todo = try req.content.decode(TodoModel.self)
        
        let responsePromise = req.eventLoop.makePromise(of: TodoModel.self)
        
        guard !inMemoryTodos.contains(where: { $0.id == todo.id }) else {
            let failure = Abort(.conflict, reason: "Todo with given ID already exists.")
            responsePromise.fail(failure)
            return responsePromise.futureResult
        }
        
        inMemoryTodos.append(todo)
        responsePromise.succeed(todo)
            
        return responsePromise.futureResult
    }
    
    func createMany(req: Request) throws -> EventLoopFuture<[TodoModel]> {
        let todos = try req.content.decode([TodoModel].self)
        
        let responsePromise = req.eventLoop.makePromise(of: [TodoModel].self)
        
        var addedTodos: [TodoModel] = []
        todos.forEach { todo in
            guard !inMemoryTodos.contains(where: { $0.id == todo.id }) else { return }
            inMemoryTodos.append(todo)
            addedTodos.append(todo)
        }
        responsePromise.succeed(addedTodos)
            
        return responsePromise.futureResult
    }
    
    func update(req: Request) throws -> EventLoopFuture<TodoModel> {
        let todo = try req.content.decode(TodoModel.self)
        
        let responsePromise = req.eventLoop.makePromise(of: TodoModel.self)
        
        if let index = inMemoryTodos.firstIndex(where: { $0.id == todo.id }) {
            inMemoryTodos[index] = todo
            responsePromise.succeed(todo)
        } else {
            responsePromise.fail(Abort(.badRequest))
        }
            
        return responsePromise.futureResult
    }
    
    func updateMany(req: Request) throws -> EventLoopFuture<[TodoModel]> {
        let todos = try req.content.decode([TodoModel].self)
        
        let responsePromise = req.eventLoop.makePromise(of: [TodoModel].self)
        
        var updatedTodos: [TodoModel] = []
        todos.forEach { todo in
            guard let index = inMemoryTodos.firstIndex(where: { $0.id == todo.id }) else { return }
            inMemoryTodos[index] = todo
            updatedTodos.append(todo)
        }
        responsePromise.succeed(updatedTodos)
        
        return responsePromise.futureResult
    }
    
    func delete(req: Request) throws -> EventLoopFuture<HTTPStatus>  {
        
        let responsePromise = req.eventLoop.makePromise(of: HTTPStatus.self)
        
        guard let id = req.parameters.get("id") else {
            responsePromise.fail(Abort(.badRequest))
            return responsePromise.futureResult
        }
        
        if let index = inMemoryTodos.firstIndex(where: { $0.id == id }) {
            inMemoryTodos.remove(at: index)
            inMemoryDeletedTodoIds.insert(id)
        }
        responsePromise.succeed(.ok)
            
        return responsePromise.futureResult
    }
    
    func deleteMany(req: Request) throws -> EventLoopFuture<HTTPStatus>  {
        let ids = try req.content.decode([String].self)
        
        let responsePromise = req.eventLoop.makePromise(of: HTTPStatus.self)
        
        inMemoryTodos.removeAll { ids.contains($0.id) }
        ids.forEach {
            inMemoryDeletedTodoIds.insert($0)
        }

        responsePromise.succeed(.ok)
            
        return responsePromise.futureResult
    }
}

extension TodosController: RouteCollection {
    
    func boot(routes: RoutesBuilder) throws {
        let todos = routes.grouped("todos")
        todos.get("", use: get)
        todos.post("", use: create)
        todos.put("", use: update)
        todos.delete(":id", use: delete)
        
        let todosBulk = todos.grouped("bulk")
        todosBulk.post("", use: createMany)
        todosBulk.put("", use: updateMany)
        todosBulk.delete("", use: deleteMany)
    }
}
