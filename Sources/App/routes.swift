import Vapor

func routes(_ app: Application) throws {
    
    // v1
    let apiV1Routes = app.grouped("api", "v1")
    try apiV1Routes.register(collection: TodosController<Todo>())
    
    // v2 - test
    let testRoutes = app.grouped("test")
    let testApiV2Routes = testRoutes.grouped("api", "v2")
    try testApiV2Routes.register(collection: TodosController<TodoV2>())
    
    // v2
    let apiV2Routes = app.grouped("api", "v2")
    try apiV2Routes.register(collection: TodosController<TodoV2>())
}
